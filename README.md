# `doc-writer`
Generate documentation in multiple formats.
For usage information see [`docs.rs`](https://docs.rs/doc-writer/latest/doc_writer/trait.DocumentationWriter.html).

Results look like [`grep.md`](./examples/grep.md) and [`man grep.1`](./examples/grep.1.txt).

## License
`doc-writer` is distributed under the terms of both the MIT license and the Apache License (Version 2.0).

See the [LICENSE-APACHE](./LICENSE-APACHE) and [LICENSE-MIT](./LICENSE-MIT) files in this repository for more information.