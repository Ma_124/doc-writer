# grep &ndash; print lines that match patterns

usage: `grep [OPTION...] PATTERNS [FILE...]`


grep searches for PATTERNS in each FILE\.
PATTERNS is one or more patterns separated by newline characters\.


A FILE of “-” stands for standard input\.


## Options
- `--help`: Output a usage message and exit\.
- `--color=auto`: Display color on the terminal


## Regular Expressions
A regular expression is a pattern that describes a set of strings\.


Perl-compatible regular expressions give additional functionality, and are documented in
**pcresyntax**(3)

## Environment
- `GREP_COLOR=auto`: Display color on the terminal


## License
Copyright 1998-2000, 2002, 2005-2020 Free Software Foundation, Inc\.  
  
This is free software; see the source for copying conditions\.  
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE\.
